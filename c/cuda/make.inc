####################################################################
#  make include file.                                              #
####################################################################
#
SHELL = /bin/sh

# ----------------------------------------------------------------------
# - gpaw-cuda Directory Structure / gpaw-cuda library --------------------
# ----------------------------------------------------------------------
#
TOPdir       = .
INCdir       = $(TOPdir)
PYTHONINCdir?=/ccsopen/home/ghouchin/miniconda3/envs/GPAWsome2/include/python3/
PYTHONLIBdir?=/ccsopen/home/ghouchin/miniconda3/envs/GPAWsome2/lib/python3.7/
NUMPYINCdir?=`python -c "import numpy; print(numpy.get_include())"`
MPIINCdir?=/autofs/nccsopen-svm1_sw/ascent/.swci/1-compute/opt/spack/20180914/linux-rhel7-ppc64le/gcc-4.8.5/spectrum-mpi-10.2.0.7-20180830-tgdfktn2snkkc7khao5zt6ibjyyolc3v/include/ 
LIBdir       = $(TOPdir)
CUGPAWLIB    = $(LIBdir)/libgpaw-cuda.a 

#
# ----------------------------------------------------------------------
# - NVIDIA CUDA includes / libraries / specifics -----------------------
# ----------------------------------------------------------------------
CUDAdir       = /sw/ascent/cuda/9.2.148
CUDAINCdir       = $(CUDAdir)/include
CUDALIBdir       = $(CUDAdir)/lib64
CUDA_OPTS     = 

#
# ----------------------------------------------------------------------
# - gpaw-cuda includes / libraries / specifics -------------------------------
# ----------------------------------------------------------------------
#

CUGPAW_INCLUDES =  -I$(INCdir) -I$(CUDAINCdir) -I$(MPIINCdir) -I$(NUMPYINCdir)  -I$(PYTHONINCdir)
CUGPAW_OPTS     = -DPARALLEL=1 -DGPAW_CUDA=1
#CUGPAW_OPTS     = -DPARALLEL=1 -DGPAW_CUDA=1 -DMPI_CUDA=1

#
# ----------------------------------------------------------------------
#

CUGPAW_DEFS     = $(CUGPAW_OPTS) $(CUDA_OPTS)  $(CUGPAW_INCLUDES)

#
# ----------------------------------------------------------------------
# - Compilers / linkers - Optimization flags ---------------------------
# ----------------------------------------------------------------------

CC           = gcc
CCNOOPT      = $(CUGPAW_DEFS)
# CCFLAGS      = $(CUGPAW_DEFS) -fomit-frame-pointer -mfpmath=sse -msse3 -mtune=native -O3 -funroll-loops -W -Wall -m64 -fPIC -std=c99
CCFLAGS      = $(CUGPAW_DEFS) -g -fPIC -m64 -O3

NVCC           = nvcc
NVCCFLAGS      = $(CUGPAW_DEFS) -O3 -g -arch=sm_70 -m64 --compiler-options '-O3 -g -fPIC' 
#NVCCFLAGS      = $(CUGPAW_DEFS) -Xptxas -v -O3 -g -arch=sm_70 -m64 --compiler-options '-O3 -g -fPIC' 
#NVCCFLAGS      = $(CUGPAW_DEFS) -Xptxas -v -O3 -g -G -arch=sm_70 -m64 --compiler-options '-O3 -g -fPIC' 



ARCH     = ar
ARCHFLAGS= cr
RANLIB   = ranlib


